# ABI breaks introduced by 64-bit `time_t` on armhf (or i386)

This repository contains scripts used to determine which libraries' ABIs are
impacted by switching to a 64-bit `time_t` on armhf.

## Architecture support

As hinted by the name of the project, the main supported architecture is armhf.
The script has been made to support i386 too but it can be broken by work
related to armhf; this can be expected due to limited resources to support all
architectures but it would also be entirely unintentional.

## Principles of operation

The main script at the moment is `check-armhf-time_t`. It uses
`abi-compliance-checker` to build the headers of dev packages in three modes:
"base", 64-bit `time_t` and LFS (which will be implied by 64-bit `time_t` with
glibc) and dump their ABIs. Another run of `abi-compliance-checker` will diff
the ABIs in order to learn which ABIs depend upon `time_t`.

## Outputs

Logs files are stored in the `logs` directory.

ABI dumps, log files and `abi-compliance-checker` inputs are stored in the
`dumps` directory as `.tar.xz` compressed archives (compression ratio reaches
100:1 so don't extract everything at once on slow media with little free
space).

Sqlite database is at `armhf-time_t-analysis.db`. It stores the dump status,
the analysis results and most quirks used during the dump step.

## Scripts

Scripts to run:
- `check-armhf-time_t`: dumps the ABIs in the three modes aforementioned,
- `diff.sh`: diff the ABIs of all packages which had their ABI dumped
- `diff_virtuals_reduce.sh`: collect the diff results of virtual packages in
  order to establish the result for the real package a whole

Helper scripts:
- `diff_list_packages_dumped.sh`: lists packages that had their ABI dumped
- `diff_package.sh`: diff the ABIs of a single package

## Goals

We want all packages to be analyzed and that means making sure none ends up in
the `failed-compilation` state.

While fixing build failures, you will probably find using `--mode=dev` helpful
since it is stricter and much faster (but uses more memory). While doing batch
analysis, you will probably prefer `--mode=batch` which is the default. This is
similar in spirit to using -Werror for development but not for releases.

You can also pass --help to `check-armhf-time_t` to get more detailled usage.

## Example

`cat package_list | xargs ./check-armhf-time_t --mode-dev --silent-apt`

## Known issues
- The `--skip` option for ABI dump can be a bit tricky to use
- Plumbing between the various steps for the diff part could be improved
- Diff of some dart packages uses more than 16GB of memory (more than, in the
  sense that my machine OOM'ed)
- The diff step uses GNU parallel to improve runtime and it has a --memfree
  option but it might not be appropriate when some of the workloads use two
  order of magnitude more memory than others
- 

#!/usr/bin/python3

"""Determine list of binary packages that ship header files and print them on stdout."""

# TODO: Add some documentation
# pylint: disable=missing-docstring

import logging
import os
import pathlib
import re
import subprocess
import tempfile
import typing
from urllib.request import urlretrieve

import apt_pkg

REPO = "https://deb.debian.org/debian"
UNSTABLE = f"{REPO}/dists/unstable"
CONTENTS_FILES = {
    f"{UNSTABLE}/{section}/Contents-{arch}.gz": f"contents-unstable-{section}-{arch}"
    for section in ["main", "contrib", "non-free", "non-free-firmware"]
    for arch in ["all", "armhf"]
}
PACKAGES_FILES = {
    f"{UNSTABLE}/{section}/binary-{arch}/Packages.gz": f"packages-unstable-{section}-{arch}"
    for section in ["main", "contrib", "non-free", "non-free-firmware"]
    for arch in ["armhf"]
}
HEADER_PATTERN = re.compile(r"\.h(pp|xx|h)?$")
HEADER_EXCLUDE_PATTERN = re.compile("^usr/share/doc")
LIBRARY_PATTERN = re.compile(r"\.so(\.|$)")


class Contents(dict[str, set[str]]):
    _LINE_RE = re.compile(r"^(.*\S)\s+(\S+)$")
    _PACKAGES_RE = re.compile(
        r"^(?:(?:contrib|non-free(?:-firmware)?)/)?[-a-z0-9]+/([-+.a-z0-9]+)$"
    )

    def __init__(self) -> None:
        self.logger = logging.getLogger(__name__)

    @classmethod
    def _parse_list_of_packages(cls, packages_row: str) -> set[str]:
        packages = set()
        for package in packages_row.split(","):
            match = cls._PACKAGES_RE.match(package)
            if not match:
                logger = logging.getLogger(__name__)
                logger.error("Failed to parse '%s' from '%s'", package, packages_row)
                continue
            packages.add(match.group(1))
        return packages

    @classmethod
    def from_file(cls, contents_file: typing.TextIO) -> typing.Self:
        # Format: filename, space(s), section, slash, binary package
        logger = logging.getLogger(__name__)
        contents = cls()
        for line in contents_file:
            match = cls._LINE_RE.match(line)
            if not match:
                logger.error("Failed to parse: %s", line)
                continue
            contents[match.group(1)] = cls._parse_list_of_packages(match.group(2))
        return contents

    def find_packages(
        self,
        filename_re: re.Pattern[str],
        exclude_re: typing.Optional[re.Pattern[str]] = None,
    ) -> set[str]:
        found = set()
        excluded = set()
        for filename, package in self.items():
            if filename_re.search(filename):
                if exclude_re and exclude_re.search(filename):
                    excluded |= package
                    continue
                found |= package
        if exclude_re:
            self.logger.info("Excluded %i packages.", len(excluded - found))
        return found

    def find_packages_with_headers(self) -> set[str]:
        packages = self.find_packages(HEADER_PATTERN, HEADER_EXCLUDE_PATTERN)
        self.logger.info("Found %s packages with header files.", len(packages))
        return packages

    def find_packages_with_libraries(self) -> set[str]:
        packages = self.find_packages(LIBRARY_PATTERN)
        self.logger.info("Found %s packages with library files.", len(packages))
        return packages


class SourceMapping(dict[str, str]):
    """Map binary Debian package names to source package names and
    mirror paths."""

    def __new__(cls, *args: typing.Any, **kwargs: typing.Any) -> typing.Self:
        logger = logging.getLogger(__name__)
        logger.info("Initialising apt_pkg...")
        apt_pkg.init()
        return super().__new__(cls, *args, **kwargs)

    def add_packages_file(self, filename: str) -> None:
        logger = logging.getLogger(__name__)
        logger.info("Parsing %s...", filename)
        current_entries = len(self)
        with apt_pkg.TagFile(filename) as tag_file:
            for section in tag_file:
                package = section["Package"]
                # Strip possible appended version
                source = section.get("Source", package).split(" ", maxsplit=1)[0]
                filename = section.get("Filename")
                if package in self and source != self[package]:
                    logger.error(
                        "Source package map '%s' -> '%s' already in mapping,"
                        " ignoring '%s' -> '%s'",
                        package,
                        self[package],
                        package,
                        section.get("Source", package),
                    )
                    continue
                self[package] = (source, filename)
        logger.info("Added %i entries.", len(self) - current_entries)

    @classmethod
    def from_packages_file(cls, filename: str) -> typing.Self:
        source_mapping = cls()
        source_mapping.add_packages_file(filename)
        return source_mapping

    def extract_shlibs_from_binary(self, binary):
        """Find shlibs for the named binary package.  This involves
        downloading and unpacking the package, and parsing the shlibs
        control file.

        We simplify and optimize by parsing only the first word of the
        specified dependency, ignoring or'ed or and'ed dependencies (and the
        version of versioned dependencies) as for reverse-dependency
        calculation we only need one package."""
        logger = logging.getLogger(__name__)

        filename = self[binary][1]
        url = f"{REPO}/{filename}"
        logger.info("downloading %s" % url)

        shlibs = set()
        with tempfile.TemporaryDirectory() as tempdir:
            debfile = os.path.join(tempdir, "this.deb")
            urlretrieve(url, debfile)
            if subprocess.call(["dpkg-deb", "-e", debfile, tempdir]):
                logger.error(f"Could not extract deb for {binary}")
                raise OSError
            if not os.path.exists(os.path.join(tempdir, "shlibs")):
                logger.info(f"No shlibs found for {binary}")
                return None
            with open(os.path.join(tempdir, "shlibs")) as f:
                for line in f:
                    if line.startswith("#") or line.startswith("udeb:"):
                        continue
                    shlibs.add(line.split()[2])
        return shlibs

    def shlibs_for_sources(self, binary_packages):
        """Given a list of binary packages containing libraries, generate a
        dict of source packages to shlibs"""
        sources = {}
        for binary_package in binary_packages:
            shlibs = self.extract_shlibs_from_binary(binary_package)
            if not shlibs:
                continue
            try:
                sources[self[binary_package][0]].update(shlibs)
            except KeyError:
                sources[self[binary_package][0]] = shlibs

        return sources

    def map_binary_packages_to_sources(
        self, binary_packages: typing.Iterable[str]
    ) -> set[str]:
        """Map the given list of binary packages to their source package names."""
        sources = set()
        for binary_package in binary_packages:
            try:
                sources.add(self[binary_package][0])
            except KeyError:
                # can happen if Contents and Packages are out of sync;
                # any package missing must not be relevant for library
                # transitions!
                pass
        return sources

    def filter_packages(
        self, binary_packages: typing.Iterable[str], source_packages: set[str]
    ) -> set[str]:
        """Filter the given list of binary packages.

        Only return the binary package where the corresponing source
        package name is in the given list of source package names.
        """
        packages = set()
        for binary_package in binary_packages:
            source_package = self.get(binary_package)
            if source_package is None:
                logger = logging.getLogger(__name__)
                logger.error(
                    "Failed to determine source package name for '%s'. Skipping.",
                    binary_package,
                )
                continue
            if source_package[0] in source_packages:
                packages.add(binary_package)
        return packages


def download_file(url: str, filename: str) -> None:
    if pathlib.Path(filename).exists():
        return
    logger = logging.getLogger(__name__)
    compressed_filename = pathlib.Path(f"{filename}.gz")
    logger.info("Downloading %s to %s...", url, compressed_filename)
    if not compressed_filename.exists():
        urlretrieve(url, compressed_filename)
    logger.info("Extracting %s to %s...", compressed_filename, filename)
    subprocess.check_call(["gunzip", str(compressed_filename)])


def read_contents(url: str, filename: str) -> Contents:
    logger = logging.getLogger(__name__)
    download_file(url, filename)
    logger.info("Parsing %s...", filename)
    with open(filename, encoding="utf-8") as contents_file:
        contents = Contents.from_file(contents_file)
    logger.info("Read %i files from %s.", len(contents), filename)
    return contents


def read_package_files() -> SourceMapping:
    source_mapping = SourceMapping()
    for url, filename in PACKAGES_FILES.items():
        download_file(url, filename)
        source_mapping.add_packages_file(filename)
    return source_mapping


def find_packages() -> typing.Tuple[set[str], set[str]]:
    logger = logging.getLogger(__name__)
    header_packages = set()
    library_packages = set()
    for url, filename in CONTENTS_FILES.items():
        contents = read_contents(url, filename)
        header_packages |= contents.find_packages_with_headers()
        library_packages |= contents.find_packages_with_libraries()
    logger.info(
        "Found %i packages with headers and %i packages with libraries in total.",
        len(header_packages),
        len(library_packages),
    )
    return header_packages, library_packages


def main() -> None:
    """Determine list of binary packages that ship header files and print them on stdout."""
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    source_mapping = read_package_files()
    header_packages, library_packages = find_packages()

    sources_with_libraries = source_mapping.map_binary_packages_to_sources(
        library_packages
    )

    sources_with_headers = source_mapping.map_binary_packages_to_sources(
        header_packages
    )

    header_packages_with_libraries = source_mapping.filter_packages(
        header_packages, sources_with_libraries
    )

    shlibs_for_sources = source_mapping.shlibs_for_sources(
        source_mapping.filter_packages(library_packages, sources_with_headers)
    )
    logger.info(
        "Found %s packages with header files and corresponing libraries"
        " (removed %i packages without corresponing library).",
        len(header_packages_with_libraries),
        len(header_packages) - len(header_packages_with_libraries),
    )

    # hard-code some mappings where we know which -dev packages go with
    # which runtime libs, and matching on all runtime libs from the source
    # package would be problematically over-broad
    special_cases = {
        'libminizip-dev': 'libminizip1',
        'zlib1g-dev': 'zlib1g',
        'libmariadbd-dev': 'libmariadbd19',
        'libcephfs-dev': 'libcephfs2',
        'librados-dev': 'librados2',
        'libradospp-dev': 'librados2',
        'libradosstriper-dev': 'libradosstriper1',
        'librbd-dev': 'librbd1',
        'librgw-dev': 'librgw2',
        'libsqlite3-mod-ceph-dev': '',
        'rados-objclass-dev': '',
        'codeblocks-common': '',
        'codeblocks-dev': 'libcodeblocks0',
        'libwxsmithlib-dev': 'libwxsmithlib0',
        'libldb-dev': 'libldb2',
        'libsmbclient-dev': 'libsmbclient',
        'libwbclient-dev': 'libwbclient0',
        'python3-ldb-dev': 'python3-ldb',
        'samba-dev': 'samba-libs python3-samba',
        'libelektra-dev': 'libelektra4',
        'libcamel1.2-dev': 'libcamel-1.2-64',
        'libebackend1.2-dev': 'libebackend-1.2-11',
        'libebook-contacts1.2-dev': 'libebook-contacts-1.2-4',
        'libebook1.2-dev': 'libebook-1.2-21',
        'libecal2.0-dev': 'libecal-2.0-2',
        'libedata-book1.2-dev': 'libedata-book-1.2-27',
        'libedata-cal2.0-dev': 'libedata-cal-2.0-2',
        'libedataserver1.2-dev': 'libedataserver-1.2-27',
        'libedataserverui1.2-dev': 'libedataserverui-1.2-4',
        'libedataserverui4-dev': 'libedataserverui4-1.0-0',
        'libgfs-dev': 'libgfs-1.3-2',
        'libgnunet-dev': 'libgnunet0.20',
        'inn2-dev': 'inn2',
        'libkompareinterface-dev': 'libkompareinterface5',
        # the -dev package for this lib is libgirepository1.0-dev
        'gobject-introspection': '',
        # the -dev package for this lib is libdbus-1-dev
        'dbus-tests': '',
        # trust that libstdc++ has handled the transition correctly already
        'libstdc++-13-dev': '',
        'libgm2-13-dev': 'libgm2-18',
        'libgfortran-13-dev': 'libgfortran5',
        'libgccjit-13-dev': 'libgccjit0',
        'gnat-13': 'libgnat-13',
        'gfortran-13': 'libgfortran5',
        'gcc-13-plugin-dev': '',
        'gcc-12-plugin-dev': '',
        'gcc-11-plugin-dev': '',
        'gcc-10-plugin-dev': '',
        'gcc-9-plugin-dev': '',
        # manual inspection of libboost1.74-dev report shows only
        # boost::chrono affected; assume this is true for other versions
        'libboost1.74-dev': 'libboost-chrono1.74.0',
        'libboost1.81-dev': 'libboost-chrono1.81.0',
        'libboost1.83-dev': 'libboost-chrono1.83.0',
        # the -dev package for this lib is libcrypt-dev
        'libxcrypt-source': '',
        # break down the mapping of util-linux libs
        'libblkid-dev': 'libblkid1',
        'libfdisk-dev': 'libfdisk1',
        'libmount-dev': 'libmount1',
        'libsmartcols-dev': 'libsmartcols1',
        'uuid-dev': 'libuuid1',
        # manually confirmed to not be ABI-breaking (av_timegm is public
        # but unused in the archive)
        'libavutil-dev': '',
        # false-positive, functions in headers but not in library ABI
        'libfido2-dev': '',
        # per maintainer, only breaks plugin ABI, to be handled separately
        'libvlccore-dev': '',
        # library already uses LFS
        'libmd-dev': '',
        'liburing-dev': '',
        # only these runtime libs are affected
        'libkrb5-dev': 'libkdb5-10 libgssrpc4',
        'krb5-multidev': 'libkdb5-10 libgssrpc4',
        # false-positive due to a dependent lib's headers
        'libkrad-dev': '',
        # one-off of a library with a comma-separated shlibs.  Don't try to
        # cleverly generalize.
        'libmrss0-dev': 'libmrss0',
        # Provides: complicates handling but it has no revdeps anyway
        'libjpeg62-dev': '',
        # The 'removed' symbols are glibc symbols; ignore
        'libfltk1.3-compat-headers': '',
        # jni and python module with incorrect shlibs
        'liblink-grammar-dev': 'liblink-grammar5',
        'libipa-hbac-dev': 'libipa-hbac0',
        'libvirtualpg-dev': 'libvirtualpg0',
        # runtime lib package has no ABI information and no revdeps
        '389-ds-base-dev': '',
        'aircrack-ng': '',
        'libapt-pkg-dev': 'libapt-pkg6.0',
        'asterisk-dev': '',
        'atheme-services': '',
        'avfs': '',
        'bind9-dev': '',
        'binutils-dev': 'libsframe1 libctf0 libctf-nobfd0 libbinutils',
        'cairo-dock-dev': '',
        'calamares': '',
        'choqok': '',
        'code-saturne-include': '',
        'criu': '',
        'cyrus-dev': '',
        'deepin-terminal': '',
        'dovecot-dev': '',
        'duma': '',
        'eclipse-titan': '',
        'emboss-lib': '',
        'fswatch': '',
        'gammaray-dev': '',
        'garmin-forerunner-tools': '',
        'gcli': '',
        'geany-common': '',
        'ggobi': '',
        'glabels-dev': '',
        'gnome-software-dev': '',
        'gnumeric': '',
        'go-for-it': '',
        'gpsim-dev': '',
        'guile-cairo-dev': '',
        'htdig': '',
        'httest': '',
        'janus-dev': '',
        'kconfig-frontends': '',
        'kconfig-frontends-nox': '',
        'kdepim-addons': '',
        'kdevelop-data': '',
        'kdevelop-dev': '',
        'kea-dev': '',
        'kio-audiocd-dev': '',
        'lepton-eda': '',
        'libclips-dev': '',
        'android-libart': '',
        'android-libboringssl-dev': '',
        'android-libselinux-dev': '',
        'android-libsepol-dev': '',
        'android-libunwind-dev': '',
        'android-libext4-utils-dev': '',
        'android-libfec-dev': '',
        'android-libandroidfw-dev': '',
        'android-libbase-dev': '',
        'android-libcutils-dev': '',
        'android-liblog-dev': '',
        'android-libnativehelper-dev': '',
        'android-libsparse-dev': '',
        'android-libutils-dev': '',
        'android-libziparchive-dev': '',
        'android-platform-system-core-headers': '',
        'python3-talloc-dev': '',
    }
    for llvm in ('14', '15', '16', '17'):
        override = ''
        for llvmlib in ('libc++1-', 'libc++abi1-', 'libclang-cpp',
                        'libclang1-', 'liblldb-', 'libllvm', 'libmlir-',
                        'libomp5-', 'libunwind-'):
           override += llvmlib + llvm + ' '
        for llvmdev in ('libc++-', 'libclang-', 'libclang-rt-', 'libclc-',
                        'liblld-', 'liblldb-', 'libmlir-', 'libunwind-',
                        'llvm-'):
	        special_cases[llvmdev + llvm + '-dev'] = override

    for package in sorted(header_packages_with_libraries):
        if package in special_cases:
            print("%s: %s" % (package, special_cases[package]))
            continue

        try:
            print("%s: %s" \
                  % (package,
                     ' '.join(shlibs_for_sources[source_mapping[package][0]])))
        except KeyError:
            # source package ships both .h and .so but no shlibs, so exclude
            pass


if __name__ == "__main__":
    main()
